# State transitions


`ActionItem`s have a state.

The state transitions are as defined in VDA5050 spec.

![state transitions](img/Figure14.png)
