#!/usr/bin/env python3
"""
 Example on programmaticallly creating an action chain.

 Copyright (c) 2024 ROX Automation - Jev Kuznetsov
"""

import asyncio
import coloredlogs

from action_trees import ActionItem
from action_trees import LOG_FORMAT


class ChildAction(ActionItem):
    """action that runs for a while"""

    def __init__(self, name: str, duration: float) -> None:
        super().__init__(name)
        self._duration = duration

    async def _on_run(self) -> None:
        """Run the action item."""
        await asyncio.sleep(self._duration)


class MainAction(ActionItem):
    """main action that runs a few children"""

    def __init__(self) -> None:
        super().__init__("main")

        for idx in range(10):
            self.add_child(ChildAction(name=f"child_{idx}", duration=1))

    async def _on_run(self) -> None:

        for child in self.children:
            await child.start()


async def main() -> None:

    action = MainAction()

    # start task in background
    action_task = action.start()

    # wait a bit and cancel maint task, before all children are done
    await asyncio.sleep(5)

    try:
        await action.cancel()
        await action_task
    except asyncio.CancelledError:
        print("cancelled")
    finally:
        action.display_tree()


if __name__ == "__main__":
    coloredlogs.install(
        level="DEBUG",
        fmt=LOG_FORMAT,
    )

    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print("interrupted")
